const fs = require('fs');

const filename = './db.json';

let data = [];

module.exports = {
  init() {
    try {
      const fileContents = fs.readFileSync(filename);
      data = JSON.parse(fileContents);
    } catch (e) {
      data = [];
    }
  },
  getItems(date) {
    if(date) {
      return data.filter(message => message.date > date);
    } else {
      return data.slice(data.length -30);
    }
  },
  addItem(item) {
    const date = new Date().toISOString();
    item.date = date;
    data.push(item);
    this.save(item);
  },
  save() {
    fs.writeFileSync(filename, JSON.stringify(data, null, 2));
  }
};
