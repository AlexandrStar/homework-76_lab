const express = require('express');
const fileDb = require('../fileDb');
const nanoid = require('nanoid');

const router = express.Router();

router.get('/', (req, res) => {
  res.send(fileDb.getItems(req.query.datetime));
});

router.post('/', (req, res) => {

  if (!req.body.message || !req.body.author) {
    res.status(400).send({error: "Author and message must be present in the request"})
  } else {
    const message = req.body;
    message.id = nanoid();
    fileDb.addItem(message);
    res.send({message: 'OK'});
  }

});

module.exports = router;