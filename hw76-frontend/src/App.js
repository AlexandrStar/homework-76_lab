import React, { Component } from 'react';
import './App.css';
import Post from "./components/Post/Post";
import Form from "./components/Form/Form";
import {connect} from "react-redux";
import {createPost, postList} from "./store/actions/actionMessage";

class App extends Component {

  state = {
    message: '',
    author: '',
    isLoading: true
  };

  componentDidMount() {
    this.props.postList();
    this.setState({isLoading: false});
  };

  componentDidUpdate(){
    clearInterval(this.interval);
    const lastDatetime = this.props.posts.length > 1 && this.props.posts[this.props.posts.length -1].date;
    if (lastDatetime) this.interval = setInterval(this.props.postList, 3000, lastDatetime);
  };

  valueChanged = event => {
    event.preventDefault();
    const {name, value} = event.target;
    this.setState({[name]: value});
  };

  messageHandler = event => {
    event.preventDefault();
    const postData = {
      message: this.state.message,
      author: this.state.author,
    };
    this.props.createPost(postData);
  };

  mapPosts = () => {
    return this.props.posts.map((post, key) => {
      return <Post key={key} value={post.message} author={post.author} datetime={post.date}/>
    })
  };

  render() {
    return (
      <div className="container App">
      {this.state.isLoading ? <div id="preloader">Preloader</div>:
        <div className="Posts">{this.props.posts.length > 0? this.mapPosts().reverse() : "Whait!!!"}</div>}
          <Form
          message={this.state.message}
          author={this.state.author}
          change={this.valueChanged}
          onClick={this.messageHandler}
          />
      </div>
    );
  }
}

const mapStateToProps = state => {
    return {
      posts: state.mess.posts,
    };
};

const mapDispatchToProps = dispatch => {
    return {
      postList: (date) => dispatch(postList(date)),
      createPost: postData => dispatch(createPost(postData))
    };
};

export default connect(mapStateToProps, mapDispatchToProps)(App);
