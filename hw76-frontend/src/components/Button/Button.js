import React, {PureComponent} from 'react';

import './Button.css';

class Button extends PureComponent {

  render() {
    return (
      <button
        onClick={this.props.onClick}
        className={['Button', this.props.btnType].join(' ')}
      >
        {this.props.children}
      </button>
    )}
}

export default Button;