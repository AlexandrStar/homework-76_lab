import {GET_FAILURE, GET_SUCCESS} from "../actions/actionType";


const initialState = {
  message: '',
  author: '',
  posts: [],
};
const messageReducer = (state = initialState, action) => {
  switch (action.type) {
    case GET_SUCCESS:
      return {...state, posts: state.posts.concat(action.posts)};
    case GET_FAILURE:
      return state;
    default:
        return state;
  }
};

export default messageReducer;