import axios from '../../axios-api';
import {GET_FAILURE, GET_REQUEST, GET_SUCCESS} from "./actionType";

export const getRequest = () => {return {type: GET_REQUEST}};
export const getSuccess = posts => {return {type: GET_SUCCESS, posts}};
export const getFailure = error => {return {type: GET_FAILURE, error}};

export const postList = (dateTime) => {
  return dispatch => {
    dispatch(getRequest());
    const getMessegesUrl = 'messages';
    const getLastMessagesUrl = 'messages?datetime=' + dateTime;
    console.log(getLastMessagesUrl);
    axios.get(dateTime ? getLastMessagesUrl : getMessegesUrl).then(response => {
        dispatch(getSuccess(response.data));
      },
      error => dispatch(getFailure(error))
    );
  }
};

export const createPost = (postData) => {
  return dispatch => {
    axios.post('messages', postData).then(null, error => {
      console.log(error)
    })
  }
};